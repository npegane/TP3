// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookie = require('cookie');
var cookieSession = require('cookie-session');
var crypto = require('crypto');
var SHA256 = require('crypto-js/sha256');
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	//console.dir(req.token.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public')/*,new cookieSession({
    name : 'session',
    keys : ['key1','key2'],
    maxAge : 24 * 60 * 60 * 1000
}
)*/);

app.get("/users", function(req, res, next) {
	db.all('SELECT * FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all("SELECT * FROM sessions", function(err, data) {
		res.json(data);
	});
});

app.get("/ip", function(req,res){
	res.writeHead(200,{"Content-Type": "text/html;charset=UTF-8"});
	res.write(req.connection.remoteAddress);
});

app.post("/login", function(req, res, next) {
	db.all("SELECT * FROM users where ident = ?  AND password = ? ;",[req.body.login,req.body.password], 
	
	function(err, data){
		if(data.length==0){
			status = false;
		} else {
			status = true;
			var token = makeid(req.body.login,req.connection.remoteAddress);
			db.all("SELECT * FROM sessions WHERE ident = ?;",[req.body.login],function(err, data){
				if(typeof(data)===undefined || data.length == 0){
					db.run("INSERT INTO sessions VALUES(?,?);",[req.body.login,token]);
				} else {
					db.run("UPDATE sessions SET token = ? WHERE ident = ?;",[token,req.body.login]);
				}
			});			
		}

		// Génération du cookie pour y mettre le token 

		//res.cookie.token = token;
		res.setHeader('Set-Cookie', cookie.serialize('token', token, {
			httpOnly: true,
			maxAge: 60 * 60 * 24 * 7 // 1 week 
    	}));

		res.json({
			status:status,
			token : token
		})
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});

function makeid(login, ip) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < 5; i++){
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	text += ip;
	text += login;
	var hash = SHA256(text).toString();
	return hash;
}